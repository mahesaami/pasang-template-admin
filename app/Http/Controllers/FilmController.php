<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Film;
use App\Cast;
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listfilm = Film::all();
        return view('film.index', compact('listfilm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listcast = cast::all();
        return view ('film.create', compact('listcast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'judul' => 'required',
          'ringkasan' => 'required',
          'tahun' => 'required',
          'cast_id' => 'required',
          'poster' => 'required|mimes:jpeg,jpg,png|max:2200',
        ]);

        $poster = $request->poster;
        $new_poster = time() . ' - ' . $poster->getClientOriginalName();

        $film = Film::create([
          'judul' => $request->judul,
          'ringkasan' => $request->ringkasan,
          'tahun' => $request->tahun,
          'cast_id' => $request->cast_id,
          'poster' => $new_poster,
        ]);

        $poster->move('uploads/film/', $new_poster);
        return redirect('/film/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::findorfail($id);
        $listcast = cast::all();
        return view('film.edit', compact('listcast', 'film'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'judul' => 'required',
        'ringkasan' => 'required',
        'tahun' => 'required',
        'cast_id' => 'required',
        'poster' => 'required|mimes:jpeg,jpg,png|max:2200',
      ]);

      $film = Film::findorfail($id);

      If ($request->has('poster')) {
        $path = "uploads/film";
        File::delete($path . $film->poster);
        $poster = $request->poster;
        $new_gambar = time() . ' - ' . $poster->getClientOriginalName();
        $poster->move('uploads/film', $new_gambar);
        $post_data = [
          'judul' => $request->judul,
          'ringkasan' => $request->ringkasan,
          'tahun' => $request->tahun,
          'cast_id' => $request->cast_id,
          'poster' => $new_gambar,
        ];
      } else {
        $post_data = [
          'judul' => $request->judul,
          'ringkasan' => $request->ringkasan,
          'tahun' => $request->tahun,
          'cast_id' => $request->cast_id,
        ];
      }


      $film->update($post_data);
      return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
