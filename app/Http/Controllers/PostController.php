<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class PostController extends Controller
{
    public function create() {
      return view('posts.create');
    }

    public function store(Request $request) {

      $request -> validate([
        'nama' => 'required|unique:cast'
      ]);

      // $query = DB::table('cast')->insert([
      //     "nama" => $request["nama"],
      //     "umur" => $request["umur"],
      //     "bio" => $request["bio"]
      //   ]);
      //   return redirect('/posts');

      // $cast = new Cast;
      // $cast->nama = $request ["nama"];
      // $cast->umur = $request["umur"];
      // $cast->bio = $request["bio"];
      // $cast->save();
      //
      // return redirect('/posts')->with('success', 'berhasil disimpan!');

      $cast = Cast::create([
        "nama" => $request["nama"],
        "umur" => $request["umur"],
        "bio" => $request["bio"]
      ]);
      return redirect('/posts')->with('success', 'berhasil disimpan!');
    }

    public function index() {
      // $listcast = DB::table('cast')->get();
      $listcast = cast::all();
      return view('posts.index',compact('listcast'));
    }

    public function show($id) {
      $cast = DB::table('cast')->where('id', $id)->first();
      return view('posts.show',compact('cast'));
    }

    public function edit($id) {
      $cast = DB::table('cast')->where('id', $id)->first();
      return view('posts.edit',compact('cast'));
    }

    public function update(Request $request, $id) {

      $request -> validate([
        'nama' => 'required|unique:cast'
      ]);

      $query = DB::table('cast')
           ->where('id', $id)
           ->update([
               'nama' => $request["nama"],
               'umur' => $request["umur"],
               'bio' => $request["bio"]
           ]);
       return redirect('/posts');
    }

    public function destroy($id) {
      $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/posts');
    }
  }
