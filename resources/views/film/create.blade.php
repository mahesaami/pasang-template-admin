@extends('master')

@section('title')
Tambah Film
@endsection

@section('body')
<form  action="/film" method="post" enctype="multipart/form-data">
  @csrf
<div class="card-body">
  <div class="form-group">
    <label for="judul">Judul Film</label>
    <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Film">
  </div>
  <div class="form-group">
    <label for="ringkasan">Ringkasan</label>
    <textarea id="ringkasan" class="form-control" name="ringkasan" rows="10" cols="50">
      </textarea>
  </div>
  <div class="form-group">
    <label for="tahun">Tahun</label>
    <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Tahun">
  </div>
  <div class="form-group">
    <label for="poster">Poster</label>
    <input type="file" class="form-control" id="poster" name="poster">
  </div>
  <div class="form-group">
    <label for="cast">Cast</label>
    <select class="custom-select" name="cast_id" id="cast">
      <option value="">Silahkan Pilih Cast</option>
    @foreach ($listcast as $item)
    <option value="{{$item->id}}">{{$item->nama}}</option>
    @endforeach

    </select>

  </div>
</div>
<!-- /.card-body -->

<div class="card-footer">
  <button type="submit" class="btn btn-primary">Create</button>
</div>
</form>
@endsection
