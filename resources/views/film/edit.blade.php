@extends('master')

@section('title')
Edit Film
@endsection

@section('body')
<form  action="/film/{{$film->id}}" method="post" enctype="multipart/form-data">
  @csrf
  @method('put')
<div class="card-body">
  <div class="form-group">
    <label for="judul">Judul Film</label>
    <input type="text" class="form-control" value="{{$film->judul}}" id="judul" name="judul" placeholder="Judul Film">
  </div>
  <div class="form-group">
    <label for="ringkasan">Ringkasan</label>
    <textarea id="ringkasan" class="form-control" name="ringkasan" rows="10" cols="50">
      {{$film->ringkasan}}
      </textarea>
  </div>
  <div class="form-group">
    <label for="tahun">Tahun</label>
    <input type="number" class="form-control" value="{{$film->tahun}}" id="tahun" name="tahun" placeholder="Tahun">
  </div>
  <div class="form-group">
    <label for="poster">Poster</label>
    <input type="file" class="form-control" id="poster" name="poster">
  </div>
  <div class="form-group">
    <label for="cast">Cast</label>
    <select class="custom-select" name="cast_id" id="cast">
      <option value="">Silahkan Pilih Cast</option>
    @foreach ($listcast as $item)
    @if ($item->id === $film->id)
    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
    @else
    <option value="{{$item->id}}" >{{$item->nama}}</option>
    @endif
    @endforeach

    </select>

  </div>
</div>
<!-- /.card-body -->

<div class="card-footer">
  <button type="submit" class="btn btn-primary">Create</button>
</div>
</form>
@endsection
