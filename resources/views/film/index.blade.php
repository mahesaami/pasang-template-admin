@extends('master')

@section('title')
Tambah Film
@endsection

@section('body')
<div class="row">

@foreach($listfilm as $item)
<div class="col-4">
<div class="card" style="width: 18rem;">
    <img src="{{asset('uploads/film/'.$item->poster)}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h4 class="card-title">{{$item->judul}} ( {{$item->tahun}} )</h4>
      <p class="card-text">{{$item->ringkasan}}</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      <form action="/film/{{$item->id}}" method="POST">
        <a href="/film/{{$item->id}}" class="btn btn-info">Show</a>
        <a href="/film/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
    </div>
  </div>
  </div>
@endforeach
  </div>
@endsection
