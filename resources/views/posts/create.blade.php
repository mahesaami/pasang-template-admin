@extends('master')

@section('title')
Cast
@endsection

@section('body')
                <form  action="/posts" method="post">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                  </div>
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name="umur" placeholder="Umur">
                  </div>
                  <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea id="bio" class="form-control" name="bio" rows="10" cols="50">
                    </textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
@endsection
