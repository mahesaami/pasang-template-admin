@extends('master')

@section('title')
List Cast
@endsection

@section('body')
<form  action="/posts/{{$cast->id}}" method="post">
  @csrf
  @method('put')
<div class="card-body">
  <div class="form-group">
    <label for="nama">Nama Cast</label>
    <input type="text" class="form-control" id="nama" value="{{$cast->nama}}" name="nama" placeholder="Nama">
  </div>
  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="number" class="form-control" id="umur" value="{{$cast->umur}}" name="umur" placeholder="Umur">
  </div>
  <div class="form-group">
    <label for="bio">Bio</label>
    <textarea id="bio" class="form-control" value="{{$cast->bio}}" name="bio" rows="10" cols="50">
    </textarea>
  </div>
</div>
<!-- /.card-body -->

<div class="card-footer">
  <button type="submit" class="btn btn-primary">Update</button>
</div>
</form>

@endsection
