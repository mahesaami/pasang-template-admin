<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/table', function () {
  return view('table');
});

Route::get('/data-tables',function() {
  return view('data-tables');
});

Route::get('/posts/create', 'PostController@create');

Route::post('/posts', 'PostController@store');
Route::get('/posts', 'PostController@index');
Route::get('/posts/{cast_id}', 'PostController@show');
Route::get('/posts/{cast_id}/edit', 'PostController@edit');
Route::put('/posts/{cast_id}', 'PostController@update');
Route::delete('/posts/{cast_id}', 'PostController@destroy');

// CRUD film
Route::resource('film', 'FilmController');
